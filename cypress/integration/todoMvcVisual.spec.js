/// <reference types="cypress" />
import * as todoPage from '../pageObjects/todoPage'

describe('visual validation', () => {
    before(() => todoPage.navigate())

    beforeEach(() => cy.eyesOpen({appName: 'TAU todoMVC', batchName: 'TAU TodoMVC Hey!'}))
    afterEach(() => cy.eyesClose())

    it('should look good', () => {
        cy.eyesCheckWindow('emtpy todo list')

        todoPage.addTodo("Clean room")
        todoPage.addTodo("Lear JavaScript")

        cy.eyesCheckWindow('two todos')

        cy.get('.toggle').should('not.be.checked')
    })
})