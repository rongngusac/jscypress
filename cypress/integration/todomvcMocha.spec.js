/// <reference types="cypress" />

import * as todoPage  from "../pageObjects/todoPage";

describe('todo actions', () => {

    beforeEach(() => {
        todoPage.navigate()

        todoPage.addTodo("Clean room")
    })

    it('should add a new todo to the list', () => {
        todoPage.validateTodoText(0, "Clean room")

        cy.get('.toggle').should('not.be.checked')
    })
    
    it('should mark a todo to as completed', () => {
        cy.get('.toggle').click()
        cy.get('.view > label').should('have.css', 'text-decoration-line', 'line-through')
    })
    
    it('should clear completed todos', () => {
        cy.get('.toggle').click()

        cy.contains('Clear').click({force: true})
    
        cy.get('.todo-list').should('not.have.descendants', 'li')
    })
})


